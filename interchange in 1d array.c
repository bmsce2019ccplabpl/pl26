#include<stdio.h>

int main()
{
    int n;
    printf("Enter the number of elements in the array\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the array elements\n");
    for(int i=0;i<n;i++)
        scanf("%d",&a[i]);
        int small=a[0], small_loc=0, large=a[0], large_loc=0;
    for(int j=1;j<n;j++)
    {
        if(a[j]<small)
        {
            small=a[j];
            small_loc=j;
        }
        if(a[j]>large)
        {
            large=a[j];
            large_loc=j;
        }
    }
    int temp;
    temp=a[large_loc];
    a[large_loc]=a[small_loc];
    a[small_loc]=temp;
    printf("The array after interchanging the largest and the smallest number is\n");
    for(int k=0;k<n;k++)
        printf("%d\n",a[k]);
    return 0;
}
