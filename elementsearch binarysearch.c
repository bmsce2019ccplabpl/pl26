#include<stdio.h>

int main()
{
    int n,ele;
    printf("Enter the number of elements in the array\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the array elements\n");
    for(int i=0;i<n;i++)
    scanf("%d",&a[i]);
    printf("Enter the element to be searched\n");
    scanf("%d",&ele);
    int mid, beg=0, end=n-1, f=0;
    while(end>=beg)
    {
        mid=(beg+end)/2;
        if(a[mid]==ele)
        {
            f=1;
            printf("The element %d is found in the index %d\n",ele,mid);
            break;
        }
        else if(ele>a[mid])
            beg=mid+1;
        else
            end=mid-1;
    }
    if(f==0)
    printf("The element %d is not present in the array\n",ele);
    return 0;
}