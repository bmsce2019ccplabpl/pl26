#include<stdio.h>

int main()
{
    struct employee
    { 
        int emp_id;
        char name[20];
        int salary;
        char DOJ[20];
    };
    struct employee E;
    printf("Enter the Employee ID, Name, Salary and Date Of Joining\n");
    scanf("%d%s%d%s",&E.emp_id,E.name,&E.salary,E.DOJ);
    printf("The following are the employee details:\nEmployee ID is %d\nName is %s\nSalary is %d\nDate Of Joining is %s\n",
    E.emp_id,E.name,E.salary,E.DOJ);
    return 0;
}